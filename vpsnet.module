<?php

/**
 * @file 
 */

/**
 * Implementation of hook_init().
 */
function vpsnet_init() {
  //need to required on every page for ahah callback to work correctly
  require_once('inc/vpsnet.api.inc');
}

/**
 * Initialize API
 * 
 * @return
 *  api object
 */
function vpsnet_getinstance() {
$email = variable_get('vpsnet_email', '');
$key = variable_get('vpsnet_key', '');
$api = VPSNET::getInstance($email, $key);
return $api;
}

/**
 * Get the number of nodes
 * 
 * @param object $api
 * @param object $free [optional]
 * @return
 *  number of nodes on the account
 *  if $free == TRUE then return the ammount of free nodes 
 */
function vpsnet_unitNo($api, $free = FALSE, $uid = NULL) {
  
  if((variable_get('vpsnet_reseller', 0) && !user_access('Administer all VPS')) || isset($uid)){
    if(!isset($uid)){
	    global $user;
	    $uid = $user->uid;
    }
	  $units = $api->getNodes($uid);
  }
  else{
    $units = $api->getNodes();
  }
	$all_units = count($units);
	$free_units = 0;
	foreach($units as $unit){
		if(!$unit->virtual_machine_id) {
			$free_units = $free_units + 1;
		}
	}
	
	if($free){
		return $free_units;
	}
	elseif(!$free){
		return $all_units;
	}
}

/**
 * Get a list of the clouds
 * 
 * @param object $api
 * @return array(cloud_id => clould label)
 */
function vpsnet_getclouds($api) {
  $clouds = $api->getAvailableCloudsAndTemplates();
  foreach($clouds as $cloud) {
    $output[$cloud->cloud->id] = $cloud->cloud->label;
  }
  return $output;
}

/**
 * Get a list of templates for a cloud
 *
 * @param object $api
 * @param object $cloud_id
 * @return array(template_id => label)
 */
function vpsnet_gettemplates($api, $cloud_id) {
  $clouds = $api->getAvailableCloudsAndTemplates();
  foreach($clouds as $cloud) {
    if ($cloud->cloud->id == $cloud_id) {
      foreach ($cloud->cloud->system_templates as $template) {
        $output[$template->id] = $template->label;
      }
    }
  }
  return $output;
}

/**
 * Get the VM object Fully Loaded
 * @param number $vmid
 * @return object
 *  see http://vps.net/api/ for structure
 */
function vpsnet_get_virtualmachine($vmid) {
  $api = vpsnet_getinstance();
	if(variable_get('vpsnet_reseller', 0) && !user_access('Administer all VPS')){
  	$vms = $api->getVirtualMachines($user->uid);
  }
  else{
  	$vms = $api->getVirtualMachines();
  }
	foreach($vms as $vm){
		if($vm->id == $vmid){
		  $vm->loadFully();
      cache_set('vpsnet-'. $vmid, $vm);
      return $vm;
    }
  }
}

function vpsnet_get_console($vpsid) {
  $vm = vpsnet_get_virtualmachine($vpsid);
  return $vm->showConsole;
}

function vpsnet_get_cpu_usage($vpsid, $period = 'hourly') {
  $vm = vpsnet_get_virtualmachine($vpsid);
  return $vm->showCPUGraph($period);
}

function vpsnet_get_bandwidth_usage($vpsid, $period = 'hourly') {
  $vm = vpsnet_get_virtualmachine($vpsid);
  return $vm->showNetworkGraph($period);
}


/**
 * Implementation of hook_perm().
 */
function vpsnet_perm() {
  $perms[] = 'Administer all VPS';
  $perms[] = 'View vps info';
  $perms[] = 'Add vps';
  $perms[] = 'Edit vps';
  $perms[] = 'Delete vps';
  $perms[] = 'Add unit/node';
	$perms[] = 'Delete unit/node';

  return $perms;
}

/**
 * Implementation of hook_menu().
 * @return 
 */
function vpsnet_menu() {
  $items = array();

  $items['admin/settings/vps'] = array(
    'title' => t('VPS.NET Settings'),
    'description' => t('Settings for the VPS.NET API'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vpsnet_admin_settings'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
		'file' => 'inc/vpsnet.admin.inc',
   );

  $items['vps'] = array(
    'title' => 'VPS Admin',
    'page callback' => 'vpsnet_page_list',
    'access arguments' => array('View vps info'),
	'type' => MENU_NORMAL_ITEM,
	'weight' => 0,
	'file' => 'inc/vpsnet.user.inc',
  );

	$items['vps/list'] = array(
	  'title' => 'VPS List',
	  'page callback' => 'vpsnet_page_list',
	  'access arguments' => array('View vps info'),
		'type' => MENU_DEFAULT_LOCAL_TASK,
		'weight' => 1,
		'file' => 'inc/vpsnet.user.inc',
	);
	
	$items['vps/add'] = array(
	  'title' => 'Add VPS',
	  'page callback' => 'vpsnet_page_add',
	  'access arguments' => array('Add vps'),
		'type' => MENU_LOCAL_TASK,
		'weight' => 2,
		'file' => 'inc/vpsnet.user.inc',
	);
	
	$items['vps/unit'] = array(
	    'title' => variable_get('vpsnet_nodeterm', 'node') . ' admin',
	    'page callback' => 'vpsnet_page_unit',
	    'access arguments' => array('Add unit/node'),
	    'type' => MENU_LOCAL_TASK,
	    'weight' => 3,
			'file' => 'inc/vpsnet.user.inc',
	);
	$items['vps/%'] = array(
	  'title' => 'VPS info',
	  'page callback' => 'vpsnet_page_info',
	  'page arguments' => array(1),
	  'access arguments' => array('View vps info'),
		'type' => MENU_CALLBACK,
		'file' => 'inc/vpsnet.user.inc',
	);
	
	$items['vps/%/info'] = array(
	  'title' => 'VPS info',
	  'page callback' => 'vpsnet_page_info',
	  'page arguments' => array(1),
	  'access arguments' => array('View vps info'),
		'type' => MENU_DEFAULT_LOCAL_TASK,
		'weight' => 1,
		'file' => 'inc/vpsnet.user.inc',
	);
	
	$items['vps/%/info/overview'] = array(
	  'title' => 'Overview',
		'page callback' => 'vpsnet_page_info',
		'page arguments' => array(1),
		'type' => MENU_DEFAULT_LOCAL_TASK,
	);

  $items['vps/%/info/console'] = array(
    'title' => 'Console',
    'page callback' => 'vpsnet_page_console',
    'page arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );
	
	
	$items['vps/%/edit'] = array(
	  'title' => 'Edit VPS',
	  'page callback' => 'vpsnet_page_edit',
	  'page arguments' => array(1),
	  'access arguments' => array('Edit vps'),
		'type' => MENU_LOCAL_TASK,
		'weight' => 2,
		'file' => 'inc/vpsnet.user.inc',
	);
	
	$items['vps/%/delete'] = array(
	    'title' => 'Delete VPS',
	    'page callback' => 'vpsnet_page_delete',
	    'page arguments' => array(1),
	    'access arguments' => array('Delete vps'),
			'type' => MENU_LOCAL_TASK,
			'weight' => 3,
			'file' => 'inc/vpsnet.user.inc',
	);
	
	$items['vps/unit/delete/%'] = array(
	    'title' => 'Delete node/unit',
	    'page callback' => 'vpsnet_page_unit_delete',
	    'page arguments' => array(3),
	    'access arguments' => array('Delete unit/node'),
			'type' => MENU_CALLBACK,
			'file' => 'inc/vpsnet.user.inc',
	);
	
	$items['vps/ajax/cloudselect'] = array(
	  'page callback' => 'vpsnet_ajax_cloudselect',
		'access arguments' => array('Add vps'),
		'type' => MENU_CALLBACK,
		'file' => 'inc/vpsnet.user.inc',
	);

  return $items;
}

function vpsnet_include_js(){
  //add jquery elements for slider
  jquery_ui_add('ui.slider');
	$path = drupal_get_path('module', 'vpsnet');
	drupal_add_js($path . '/js/vpsnet.js');
}

function vpsnet_include_css() {
	$path = drupal_get_path('module', 'vpsnet');
	drupal_add_css($path . '/inc/vpsnet.css');
}

/**
 * Disable the validation so the AHAH can be returned.
 * Borrowed from the AHAH_HELPER module.
 */
function _vpsnet_ahah_disable_validation(&$form) {
  foreach (element_children($form) as $child) {
    $form[$child]['#validated'] = TRUE;
    _vpsnet_ahah_disable_validation(&$form[$child]);
  }
}

/**
 * Implementation of hook_theme().
 */
function vpsnet_theme() {
	
	return array(
	 'vpsnet_info' => array(
	   'arguments' => array('vm' => null),
		 'template' => 'vpsnet-info',
	 ),
   'vpsnet_info_tabs' => array(
    'arguments' => array('tabs' => null),
    'function' => 'vpsnet_theme_tabs',
   ),
   'vpsnet_overview' => array(
    'arguments' => array('vm' => null),
    'template' => 'vpsnet-overview',
   ),
	);
}

/**
 * Preprocess function for vps-overview
 * @param object $vars
 * @return 
 */
function vpsnet_preprocess_vpsnet_overview(&$vars) {
  $vm = $vars['vm'];
  $left[0] = array(
    'label' => t('Hostname'),
    'value' => $vm->hostname,
  );
  $left[1] = array(
    'label' => variable_get('vpsnet_nodeterm', 'node'),
    'value' => $vm->slice_count,
  );
  $left[2] = array(
    'label' => t('Initial root password'),
    'value' => $vm->password,
  );
  $left[3] = array(
    'label' => t('Status'),
    'value' => $vm->running ? t('Running') : t('Stopped'),
  );
  $right[0] = array(
    'label' => t('Domain Name'),
    'value' => $vm->domain_name,
  );
  $right[1] = array(
    'label' => t('Cluster'),
    'value' => $vm->cloud_id = 2 ? t('USA VPS Cloud') : t('UK VPS Cloud'),
  );
  $right[2] = array(
    'label' => t('Backups enabled'),
    'value' => $vm->backups ? t('Yes') : t('No'),
  );
  $right[3] = array(
    'label' => t('IP Address'),
    'value' => $vm->ip_address[0]->ip_address,
  );
  $vars['left'] = $left;
  $vars['right'] = $right;
}
