
if (Drupal.jsEnabled) {
	$(document).ready(function(){
		$("#vpsnet-form-unit fieldset").hide();
		$("#vpsnet-unitdetails fieldset").hide()
		$("#edit-vpsnet-unitno").val(1);
		$("span.unitno").prepend(1);
		$(".slider").slider({
			        range: "min",
					value: 2,
					min: 1,
					max: VPSNet_max,
					animate: true,
					slide: function(event, ui) {
						$("#edit-vpsnet-unitno").val(ui.value);
						$("span.unitno").empty();
						$("span.unitno").prepend(ui.value);
					}
				});
	
	});
}
