<?php


/**
 * @file
 * VPS NET admin interface 
 */

/**
 * Implementation of hook_admin_settings().
 */
function vpsnet_admin_settings() {

      $form['api'] = array(
        '#type' => 'fieldset',
        '#title' => t('API settings'),
         );

      $form['api']['vpsnet_email'] = array(
      '#type' => 'textfield', 
      '#title' => t('Email address'),
      '#default_value' => variable_get('vpsnet_email', ''),
      '#size' => 60,
      '#maxlength' => 64, 
      '#description' => t('The email address for your VPS.NET account'),
    );
     $form['api']['vpsnet_key'] = array(
        '#type' => 'textfield', 
        '#title' => t('API key'),
        '#default_value' => variable_get('vpsnet_key', ''),
        '#size' => 60,
        '#maxlength' => 64, 
        '#description' => t('The API key for your VPS.NET account'),
      );
      $form['terms'] = array( 
        '#type' => 'fieldset',
          '#title' => t('VPS terms'),
         );
  
        $form['terms']['vpsnet_nodeterm'] = array(
        '#type' => 'textfield', 
        '#title' => t('Node term'),
        '#default_value' => variable_get('vpsnet_nodeterm', 'node'),
        '#size' => 60,
        '#maxlength' => 64, 
        '#description' => t('Enter the term you want to use instead of node.'),
      );
      $form['reseller'] = array(  
          '#type' => 'fieldset',
            '#title' => t('Reseller'),
           );
      $form['reseller']['vpsnet_reseller'] = array(
        '#type' => 'checkbox', 
        '#title' => t('Yes'),
        '#default_value' => variable_get('vpsnet_reseller', 0),
        '#description' => t('I do want to resell the VPS.NET service, and not just manage my own account.'),
      );

    return system_settings_form($form);
    
}