<?php


/**
 * @file 
 */

/**
 * The content for vps/info
 **/
function vpsnet_page_list() {
global $user;
$api = vpsnet_getinstance();
$all_units = vpsnet_unitNo($api, FALSE);
$free_units = vpsnet_unitNo($api, TRUE);
if(variable_get('vpsnet_reseller', 0) && !user_access('Administer all VPS')){
	$vms = $api->getVirtualMachines($user->uid);
}
else{
	$vms = $api->getVirtualMachines();
}
$output .= t('Total number of @1s: @2', array('@1' => variable_get('vpsnet_nodeterm', 'node'), '@2' => $all_units));
$output .= '<br/>';
$output .= t('Unused @1s: @2', array('@1' => variable_get('vpsnet_nodeterm', 'node'), '@2' => $free_units));

// header information
$header = array(t('Name'), t('ID'), t('IP address'), t('Password'), t('Site'), t('Delete'));
$options = array('absolute' => TRUE);

foreach($vms as $vm){
  $vm->loadFully();
  foreach($vm->ip_addresses as $ip_info){
    $ip .= $ip_info->ip_address;
    $ip .= "<br/>";
  }
  
  if ($vm->running) {
    $state = t('Running');
  }
  else {
    $state = t('Stopped');
  }
  $site_name = $vm->hostname . "." . $vm->domain_name;
  $label = $vm->label .' ('. $state .')';
  $site = l($site_name, 'http://'. $site_name, $options);
  $info = l($label, 'vps/' . $vm->id . '/info');
  $delete = l('Delete', 'vps/' . $vm->id . '/delete');
  $rows[] = array($info,$vm->id,$ip,$vm->password,$site,$delete);
  unset($ip);
}
  $output .= theme('table', $header, $rows);

return $output;
}

function vpsnet_page_add(){
  //include js and css
  vpsnet_include_js();
  vpsnet_include_css();
  
  //get free nodes
  $api = vpsnet_getinstance();
  $free_units = vpsnet_unitNo($api, TRUE);
  
  //if there are no free nodes left then redirect and display a message
  if (!$free_units) {
    drupal_set_message(t('You do not have any free @1s', array('@1' => variable_get('vpsnet_nodeterm', 'node'))), 'warning');
    drupal_goto('vps/unit');
  }
  
  //set the max of the slider to the total free nodes
  $js = 'var VPSNet_max = '. $free_units .';';
  drupal_add_js($js, 'inline');
  
  $output .= drupal_get_form(vpsnet_form_add);
  return $output;
  
}

function vpsnet_form_add($form_state, $vpsid = NULL) {
  $api = vpsnet_getinstance();
  $free_units = vpsnet_unitNo($api, TRUE);
  $form = array();
  
  // Set the cache to true to create a $form_state cache on submit
  $form['#cache'] = TRUE;
  
  $vpsnet_nodeterm = variable_get('vpsnet_nodeterm', 'node');
	if ($vpsid || $form_state['values']['vpsid']) {
		if ($form_state['values']['vpsid']) {
			$vpsid = $form_state['values']['vpsid'];
		}
		$vm = vpsnet_get_virtualmachine($vpsid);
		$form['vpsid'] = array(
		  '#value' => $vpsid,
			'#type' => 'value',
		);
		
		$form_state['storage']['vpsnet_label'] = $vm->label;
		$form_state['storage']['vpsnet_hostname'] = $vm->hostname .'.'. $vm->domain_name;
		$form_state['storage']['vpsnet_backup'] = $vm->backups_enabled;
		$nodes = $vm->slice_count;
	}

  $form['vpsnet_label'] = array(
    '#type' => 'textfield', 
    '#title' => t('VPS Label'),
    '#default_value' => $form_state['storage']['vpsnet_label'],
    '#size' => 60,
    '#maxlength' => 64,
    '#required' => TRUE, 
    '#description' => t('A unique label for your VPS.'),
   );
   $form['vpsnet_hostname'] = array(
      '#type' => 'textfield', 
      '#title' => t('VPS Hostname'),
      '#default_value' => $form_state['storage']['vpsnet_hostname'],
      '#size' => 60,
      '#maxlength' => 64, 
      '#required' => TRUE,
      '#description' => t('The hostname for your VPS'),
    );
    //enable backups
    $form['vpsnet_backup'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Backups'),
      '#default_value' => $form_state['storage']['vpsnet_backup'],
      '#description' => t('Backups are $5/m extra'),
    );
    
		if (!$vpsid) {
	    $cloud_options = array(0 => '= Select =') + vpsnet_getclouds($api);    
	    $form['cloud_select'] = array(
	      '#type' => 'select',
	      '#title' => t('Cloud Location'),
	      '#default_value' => $form_state['storage']['cloud'],
	      '#options' => $cloud_options,
	      '#ahah' => array(
	        'event' => 'change',
	        'path' => 'vps/ajax/cloudselect',
	        'method' => 'replace',
	        'effect' => 'fade',
	        'wrapper' => 'vpsnet-cloud-wrapper',
	      ),
	    );
	    //Select submit handler
	    $form['cloud_select_submit'] = array(
	       '#type' => 'submit',
	       '#value' => t('Update the Cloud'),
	       '#submit' => array('vpsnet_ahah_cloud_select_submit'),
	       '#attributes' => array('class' => 'no-js'),
	    );
	    
	    //wrapper for the new content
	    $form['cloud_wrapper'] = array(
	      '#true' => TRUE,
	      '#weight' => 3,
	      '#prefix' => '<div id="vpsnet-cloud-wrapper">',
	      '#suffix' => '</div>',
	    );
	    
	    //if there is an option for cloud selected then show the template select box
	    if ($form_state['storage']['cloud_select']) {
	      $template_options = array(0 => '= Select =') + (array)vpsnet_gettemplates($api, $form_state['storage']['cloud_select']);
	    }
	    else {
	      $template_options = array(0 => '= Select a Cloud Location ');
	    }
	    $form['cloud_wrapper']['template'] = array(
	      '#type' => 'select',
	      '#title' => t('Template Type'),
	      '#options' => $template_options,
	      '#default_value' => $form_state['storage']['cloud_wrapper']['template'],
	    );
		}
    
		if ($free_units) {
	    $form['node_no'] = array(
	      '#type' => 'item',
	      '#weight' => 5,
	      '#title' => t('How many @1s do you want to add?', array('@1' => $vpsnet_nodeterm)),
	      '#value' =>   '<div class="unitno"><span class="unitno"></span> ' . 
	        $vpsnet_nodeterm . 's</div><div class="slider"></div>',
	      '#description' => t('You have @1 available @2s', array('@1' => $free_units, '@2' => $vpsnet_nodeterm)),
	    );
	    $form += vpsnet_form_unit($form_state, $nodes);
			$form['unitdetails']['#weight'] = 5;
		}
    //change the submit handler on the submit button
		$form['submit'] = array(
		  '#type' => 'submit', 
			'#value' => t('Save'),
			'#weight' => 50,
			'#submit' => array('vpsnet_vpsnet_form_submit'),
		);
    
    return $form;
}

function vpsnet_form_add_validate($form, &$form_state) {
  $api = vpsnet_getinstance();
  $free_units = vpsnet_unitNo($api, TRUE);
  if ($form_state['values']['vpsnet_units'] > $free_units) {
    form_set_error('vpsnet_units', t('You don\'t have enough @1s.', array('@1' => variable_get('vpsnet_nodeterm', 'node'))));
  }
}

function vpsnet_vpsnet_form_submit($form, &$form_state) {
  global $user;

	$api = vpsnet_getinstance();
	
  $label = $form_state['values']['vpsnet_label'];
  $hostname = $form_state['values']['vpsnet_hostname'];
  $num_nodes = $form_state['values']['vpsnet_unitno'];
  $backups_enabled = $form_state['values']['vpsnet_backup'];
  $rsync_backups_enabled = false;
  $r1_soft_backups_enabled = false;
  $cloud_id = $form_state['values']['cloud_select'];
  $template_id = $form_state['values']['template'];


	if(variable_get('vpsnet_reseller', 0) && !user_access('Administer all VPS')){
  	$consumer_id = $user->uid;
	}
	else{
		$consumer_id = false;
	}
	
	if ($form_state['values']['vpsid']) {
		$vm = vpsnet_get_virtualmachine($form_state['values']['vpsid']);
		$vm->label = $label;
		$vm->fpdn = $hostname;
		$vm->backups_enabled = $backups_enabled;
		$vm->update();
		drupal_set_message('VSP was updated', 'status');
		//refresh cache
		cache_clear_all();
		drupal_goto('vps/'. $form_state['values']['vpsid']);
	}
	else {
	  $new_vm = new VirtualMachine($label, $hostname, $num_nodes, $backups_enabled, $rsync_backups_enabled, $cloud_id, $template_id, $consumer_id);  
	  $created_vm = $api->createVirtualMachine($new_vm);
		  watchdog('vpsnet','Created VM: <pre>'.print_r($created_vm,true).'</pre>');
		if (is_object($created_vm)) {
	    drupal_set_message('VPS ' . $created_vm->hostname . '.' . $created_vm->domain_name . ' has been created.', 'status');
			drupal_goto('vps');
	  }
	  else {
	    drupal_set_message('VSP was not created', 'error');
		  watchdog('vpsnet','<pre>'.print_r($created_vm['errors']).'</pre>');
			drupal_goto('vps');
	  }
	}
}

/**
 * AHAH submit function for Select
 */
function vpsnet_ahah_cloud_select_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  unset($form_state['submit_handlers']);
  form_execute_handlers('submit', $form, $form_state);
  $form_state['storage'] = $form_values;
  $form_state['rebuild'] = TRUE;
}


function vpsnet_form_unit($form_state, $nodes = NULL) {
  $form['unitdetails'] = array(
    '#type' => 'fieldset',
    '#title' => t('@1 Details', array('@1' => variable_get('vpsnet_nodeterm', 'node'))),
    '#prefix' => '<div id="vpsnet-unitdetails">',
    '#suffix' => '</div>',
     );

    $form['unitdetails']['vpsnet_unitno'] = array(
    '#type' => 'textfield', 
    '#title' => t('Number of @1s', array('@1' => variable_get('vpsnet_nodeterm', 'node'))),
    '#default_value' => $nodes,
    '#size' => 60,
    '#maxlength' => 64, 
    '#description' => t('How many @1s do you want to add?', array('@1' => variable_get('vpsnet_nodeterm', 'node'))),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
    return $form;
}

function vpsnet_form_unit_validate($form, &$form_state) {
  if ($form_state['values']['vpsnet_unitno'] < 0) {
    form_set_error('vpsnet_unitno', t('Please select a value greater than 0.'));
  }
}

function vpsnet_form_unit_submit($form, &$form_state) {
  global $user;
  $unitno = $form_state['values']['vpsnet_unitno'];
  $api = vpsnet_getinstance();
  $consumer_id = $user->uid;
  if ( $api->addNodes($unitno, $consumer_id) )
  drupal_set_message(t('@1 @2s were added to your account.', array('@1' => $unitno, '@2' => variable_get('vpsnet_nodeterm', 'node'))));
}

/**
 * The content for vps/%/info
 **/
function vpsnet_page_info($vpsid) {
	//get the info for the vps
  $vm = vpsnet_get_virtualmachine($vpsid);
	//set the title name to the label
	drupal_set_title('VPS.NET Info for '. $vm->label);
	
	if(variable_get('vpsnet_reseller', 0) && !user_access('Administer all VPS')){
		global $user;
		if($vm->consumer_id != $user->uid){
  		return drupal_access_denied();
  	}
	}
	
	$api = vpsnet_getinstance();
	$resources = $api->getAvailableCloudsAndTemplates();
	$clouds = array();
	foreach  ( $resources as $resource ) {
		$clouds[$resource->cloud->id] = $resource->cloud->label;
	}
	
	$header = array();
  $rows = array();
	$rows[] = array(
	 array('data' => t('Hostname:')),
	 array('data' => $vm->hostname),
	 array('data' => t('Domain Name:')),
	 array('data' => $vm->domain_name), 
	);

	$rows[] = array(
	 array('data' => 'Number of '. variable_get('vpsnet_nodeterm', 'node') .'s:'),
	 array('data' => $vm->slices_count),
	 array('data' => t('Cluster:')),
	 array('data' => $clouds[$vm->cloud_id]),
	);
	
	$rows[] = array(
	 array('data' => t('Initial root password:')),
	 array('data' => $vm->password),
	 array('data' => t('Backups enabled:')),
	 array('data' => $vm->backups ? t('Yes') : t('No')),
	);
	
	$rows[] = array(
	 array('data' => t('Status:')),
	 array('data' => $vm->running ? t('Running') : t('Stopped')),
	 array('data' => t('IP Address:')),
	 array('data' => $vm->ip_addresses[0]->ip_address)
	);
	
  return theme('table', $header, $rows);
}

/**
 * Page callback for vps/%/info/console
 * @param object $vpsid
 */
function vpsnet_page_console($vpsid) {
  $console = vpsnet_get_console($vpsid);
  return $console;
}

/**
 * The content for vps/%/edit
 **/
function vpsnet_page_edit($vpsid) {
  return drupal_get_form('vpsnet_form_add',$vpsid);
}

function vpsnet_page_delete($vpsid){
  $output .= t('Are you sure you want to delete this VPS?');
    $output .= drupal_get_form(vpsnet_form_delete, $vpsid);
    return $output;
}

function vpsnet_form_delete($form_state, $vpsid) {
  $form['#redirect'] = array('vps');  
  $form['hidden'] = array('#type' => 'value', '#value' => $vpsid);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Delete'));
    return $form;
}

function vpsnet_form_delete_submit($form, &$form_state) {
  $api = vpsnet_getinstance();
  $vm = new VirtualMachine();
  $vm->id = $form_state['values']['hidden'];
  $vm->remove();
    drupal_set_message('VPS ID ' . $vm->id . ' has been deleted.');
}

function vpsnet_page_unit(){
  //include js and css
  vpsnet_include_js();
  vpsnet_include_css();
  
  //set the max of the slider
  $js = 'var VPSNet_max = 6;';
  drupal_add_js($js, 'inline');

  $output .= '<div class="unitno"><span class="unitno"></span> ' . variable_get('vpsnet_nodeterm', 'node') . 's</div>';
  $output .= '<div class="slider"></div>';
  $output .= drupal_get_form(vpsnet_form_unit);
  
  $api = vpsnet_getinstance();
  $nodes = $api->getNodes();  
  $header = array(t('ID'), t('VPS'), t('Delete'));
  foreach($nodes as $key => $node){
    $delete = l('Delete', 'vps/unit/delete/' . $key);
    $rows[] = array($node->id, $node->virtual_machine_id, $delete);
  }
  $output .= theme('table', $header, $rows);
  return $output;
}

function vpsnet_page_unit_delete($key){
  $api = vpsnet_getinstance();
  $nodes = $api->getNodes();
  if ( count($nodes) > 0 )
    $nodes[$key]->remove();
    drupal_set_message('Deleted node ID ' . $nodes[$key]->id);
  drupal_goto('vps/unit');
  
}

/**
 * Menu Callback for AHAH Form
 */
function vpsnet_ajax_cloudselect() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form['#post'] = $_POST;
  $form['#redirect'] = FALSE;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;
  _vpsnet_ahah_disable_validation($form);
  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  //get the changed elements
  $changed_elements = $form['cloud_wrapper']['template'];
  unset($changed_elements['#prefix'], $changed_elements['#suffix']);
  $javascript = drupal_add_js(NULL, NULL, 'header');
  drupal_json(array(
    'status'   => TRUE,
    'data'     => theme('status_messages') . drupal_render($changed_elements), // rebuild just the part that needs to be changed
    'settings' => call_user_func_array('array_merge_recursive', $javascript['setting']),
  ));
}
