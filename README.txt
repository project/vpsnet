This module allows you to connect your Drupal site to your VPSnet hosting account.

/* Installation */
- Setup Drupal as normal.
- Make sure Curl is installed http://www.php.net/manual/en/curl.setup.php
- Make sure the jQuery_ui module is installed http://drupal.org/project/jquery_ui
- Download this module (http://drupal.org/project/vpsnet) to your modules directory
- Enable it /admin/build/modules
- Configure it /admin/settings/vps
- + Enter your VPSnet login email address
- + Enter your VPSnet API key found on https://www.vps.net/profile
- + Choose your node term. A VPSnet server is made up of multiple nodes (which are fixed amounts ofserver resources, this can get confusing with Drupal. So we have given the ability to change the term used.
- + Choose if you want to use this module to resell VSPnet's service, or just manage your own. ***This feature if not yet working***
- goto /vps to view/edit/delete your VPSnet servers, add new ones, add nodes.

/* Issues */
If you have any issues problems this module, please create a new issue. http://drupal.org/project/issues/vpsnet
